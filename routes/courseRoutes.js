const express = require('express');
const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth');

//Route for creating a course 
router.post('/', auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.addCourse(data).then(result => res.send(result));
})

//Retrieve all courses
router.get('/all', (req, res) => {
	courseController.getAllCourses().then(result => res.send(result));
})

//Retrieve all TRUE Courses
router.get('/', (req, res) => {
	courseController.getAllActive().then(result => res.send(result));
})

//Retrieve specific course
router.get("/:id", (req,res)=>{
	courseController.specificCourse(req.params.id)
	.then(result =>{
		res.send(result)
	})
})


//Update a course
router.put('/:courseId', auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
	}else{
		res.send(false);
	}

})

//archive a course/soft delete a course

router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin){
	courseController.archiveCourse(req.params).then(result => res.send(result))
	}else{
		res.send(false)
	}
})



module.exports = router;